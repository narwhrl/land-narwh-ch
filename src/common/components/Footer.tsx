const Footer = () => {
  return (
    <div className="text-center text-neutral-500">
      <div>© {new Date().getFullYear()} by <a href="https://narwh.ch/" target="_blank" className="font-semibold hover:underline ">Narwhrl.</a></div>
      <div className="flex justify-center">
        <img className="mx-2" src="/images/nextjs-2.svg" width="50px" />
        <img className="mx-2" src="/images/tailwind-css.svg" width="30px" />
        <img className="mx-2" src="/images/vercel.svg" width="70px" />
      </div>
    </div>
  );
};

export default Footer;
