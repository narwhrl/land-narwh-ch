import { ServicesProps } from "../types/services";
import { MdAutoAwesomeMosaic as MemosIcon} from "react-icons/md";

export const SERVICES: ServicesProps[] = [
  {
    title: "Notion Blog",
    description:
      "Notion Blog Powered by Cloudflare Workers & Fruition. Currently ceased maintainance.",
    icon: "/images/notion.svg",
    socialhref: "https://notion.narwh.ch/"
  },
  {
    title: "Memos Notes",
    description:
      "A privacy-first, lightweight note-taking service. Easily capture and share your great thoughts.",
    icon: "/images/mosaic.svg",
    socialhref: "https://memos.narwh.ch/"
  },
  {
    title: "Telegraph Image Hosting",
    description:
      "Hosting my own images with Telegraph & Cloudflare. Reverse Proxy with Cloudflare Pages.",
    icon: "/images/image-bed.svg",
    socialhref: "https://imgs.narwh.dev/"
  },
  {
    title: "Uptime Kuma",
    description:
      "Hosting my own images with Telegraph & Cloudflare. Reverse Proxy with Cloudflare Pages.",
    icon: "/images/uptime-kuma.svg",
    socialhref: "https://status.stcn.link/"
  },
  {
    title: "Alist",
    description:
      "A file list program which supports multiple storages, powered by Gin and Solidjs.",
    icon: "/images/alist.svg",
    socialhref: "https://drive.narwh.ch/"
  },
  {
    title: "Music Site",
    description:
      "A static music player based on Aplayer and MetingJS.",
    icon: "/images/music-narwh-ch.svg",
    socialhref: "https://music.narwh.ch/"
  },
];
