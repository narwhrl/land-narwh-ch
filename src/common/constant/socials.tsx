import { BiAt as ThreadsIcon } from "react-icons/bi";
import {
  BsGithub as GithubIcon,
  BsInstagram as InstagramIcon,
  BsLinkedin as LinkedinIcon,
  BsRssFill as BlogIcon,
  BsTwitter as TwitterIcon,
  BsSinaWeibo as WeiboIcon,
  BsTelegram as TelegramIcon,
} from "react-icons/bs";
import { TbBrandBilibili as BiliIcon } from "react-icons/tb";

import { SocialItemProps } from "../types/socials";

const iconSize = 20;

export const SOCIAL_MEDIA: SocialItemProps[] = [
  {
    title: "Telegram",
    href: "https://t.me/narwhrl",
    icon: <TelegramIcon size={iconSize} />,
    isShow: true,
    isExternal: true,
    eventName: "Social: Telegram",
    className: "hover:text-[#2481cc]",
  },
  {
    title: "Github",
    href: "https://github.com/narwhrl",
    icon: <GithubIcon size={iconSize} />,
    isShow: true,
    isExternal: true,
    eventName: "Social: Github",
    className: "hover:text-black",
  },
  {
    title: "Twitter",
    href: "https://twitter.com/Teclast_",
    icon: <TwitterIcon size={iconSize} />,
    isShow: true,
    isExternal: true,
    eventName: "Social: Twitter",
    className: "hover:text-sky-500",
  },
  {
    title: "Weibo",
    href: "https://weibo.com/narwhrl",
    icon: <WeiboIcon size={iconSize} />,
    isShow: true,
    isExternal: true,
    eventName: "Social: Weibo",
    className: "hover:text-[#d52c2b]",
  },
  {
    title: "Bilibili",
    href: "https://space.bilibili.com/7179789",
    icon: <BiliIcon size={iconSize} />,
    isShow: true,
    isExternal: true,
    eventName: "Social: Bilibili",
    className: "hover:text-[#f06292]",
  },
  {
    title: "Email",
    href: "mailto:i@nar.im",
    icon: <ThreadsIcon size={iconSize} />,
    isShow: true,
    isExternal: true,
    eventName: "Social: Email",
    className: "hover:text-black",
  },
];
