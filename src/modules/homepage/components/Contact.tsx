"use client";

import { useState } from "react";
import { FaRegEnvelopeOpen as EmailIcon } from "react-icons/fa";
import { PiChatsDuotone as ContactIcon } from "react-icons/pi";
import { TbBrandTelegram as TelegramIcon } from "react-icons/tb";

const Contact = () => {
  const bgImage = "url('/images/bg-gradient-2.svg')";

  const [copiedEmail, setCopiedEmail] = useState("");

  const [gotoTG] = useState("");

  const handleEmailClick = (email: string) => {
    navigator.clipboard.writeText(email);
    setCopiedEmail(email);

    setTimeout(() => {
      setCopiedEmail("");
    }, 2000);

  const UrlClick = {
    
  }
  };

  return (
    <div className="space-y-4">
      <div className="flex items-center gap-2 px-3">
        <ContactIcon size={24} />
        <h2 className="font-medium text-lg md:text-xl">Get in Touch</h2>
      </div>
      <div
        className="bg-[#94d7c5] text-neutral-900 border p-8 rounded-2xl cursor-pointer space-y-5 bg-no-repeat bg-right"
        data-aos="fade-up"
        data-aos-duration="1000"
      >
        <EmailIcon size={24} className="text-neutral-800" />
        <div className="space-y-3">
          <div className="font-medium text-lg md:text-xl">Drop Me an Email</div>
          <div
            className="font-black hover:underline text-neutral-800 text-lg md:text-lg"
            onClick={() => handleEmailClick("i@nar.im")}
          >
            {copiedEmail ? "Copied!" : "i@nar.im"}
          </div>
        </div>
        <p className="text-sm md:text-base text-neutral-700 leading-relaxed">
          Your emails won't be kept waiting!
        </p>
      </div>

      <div
        className="bg-[#9fc8e9] text-neutral-900 border p-8 rounded-2xl cursor-pointer space-y-5 bg-no-repeat bg-right"
        data-aos="fade-up"
        data-aos-duration="1000"
      >
        <TelegramIcon size={35} className="text-neutral-800" />
        <div className="space-y-3">
          <div className="font-medium text-lg md:text-xl">Contact me with Telegram</div>
          <div
            className="font-black hover:underline text-neutral-800 text-lg md:text-lg"
            onClick={() => window.location.href = 'https://t.me/narwhrl'}
          >
            {gotoTG ? "Goto!" : "Telegram@narwhrl"}
          </div>
        </div>
        <p className="text-sm md:text-base text-neutral-700 leading-relaxed">
          Contact me with Telegram - your message will be private and I will reply soon!
        </p>
      </div>
    </div>
  );
};

export default Contact;
