"use client";

import Image from "@/common/components/Image";
import VerifiedIcon from "@/common/components/VerifiedIcon";

const Hero = () => {
  return (
    <header className="pt-5 space-y-4 flex flex-col items-center justify-center">
      <div
        className="p-2 rounded-full border border-2"
        data-aos="flip-left"
        data-aos-duration="1000"
      >
        <Image
          src="/images/avatar.jpg"
          alt="avatar"
          width={100}
          height={100}
          rounded="rounded-full"
          className="lg:hover:scale-105"
        />
      </div>
      <div className="flex flex-col justify-center items-center space-y-3">
        <div className="flex gap-2 items-center text-center">
          <h1 className="text-2xl font-bold">Narwhrl</h1>
          <VerifiedIcon />
        </div>
        <a className="font-medium text-center text-[15px] md:text-base mx-1 leading-relaxed">
          NodeJS / React hobbyist
        </a>
        <a className="text-center text-[15px] md:text-base mx-1 leading-relaxed">
          Beware beginning & Keep learning.
          <br></br>Constantly writing interesting contents.
        </a>
      </div>
    </header>
  );
};

export default Hero;
