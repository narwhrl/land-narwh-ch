import React, { useState } from "react";

import Image from "@/common/components/Image";
import Qris from "@/common/components/Qris";

import PaymentMethodButton from "./PaymentMethodButton";

const BuyACoffee: React.FC = () => {
  const [showQris, setShowQris] = useState(false);

  const handleShowQris = () => {
    setShowQris(!showQris);
  };

  return (
    <div className="p-6 flex flex-col justify-center items-center space-y-3">
      {!showQris ? (
        <div className="flex flex-col w-full justify-center items-center space-y-3">
          <button
            className="flex justify-center border border-solid w-full px-5 py-3 rounded-xl hover:bg-gray-100"
            onClick={handleShowQris}
          >
            <Image
              src="/images/WeChat.png"
              alt="WechatPay"
              width={100}
              height={100}
            />
          </button>
          <PaymentMethodButton
            href="https://qr.alipay.com/fkx17478ay17ztw7tzkjre8"
            imageSrc="/images/Alipay_logo.png"
            altText="Alipay"
          />
          <PaymentMethodButton
            href="https://paypal.me/narwhrl"
            imageSrc="/images/paypal.webp"
            altText="PayPal"
          />
          <PaymentMethodButton
            href="https://afdian.net/a/narwhrl/plan"
            imageSrc="/images/afdian.png"
            altText="Afdian"
          />
        </div>
      ) : (
        <Qris onBack={handleShowQris} />
      )}
    </div>
  );
};

export default BuyACoffee;
