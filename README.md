<div align="center">
  <h1>aulianza.com</h1>
  <p>🌀 Personal landing page built with Next.js 13 App Router, TypeScript and Tailwind CSS.</p>
</div>
<br />

![3-devices-white](https://github.com/aulianza/aulianza.com/assets/15605885/2d1079d4-973b-417e-8b6d-d5ae93831dc7)
<br />

## Introduction

This website was built from scratch using Next.js 13 App Router and was first initialized in August 2023. It will undergo regular updates in the future.
<br /><br />

## Performance

### PageSpeed Insights : Desktop

Report URL: https://pagespeed.web.dev/analysis/https-aulianza-com/9626wfhfal?form_factor=desktop

![image](https://github.com/aulianza/aulianza.com/assets/15605885/1d67a286-eb47-41f7-9f62-d47875b4ff1b)

### PageSpeed Insights : Mobile

Report URL: https://pagespeed.web.dev/analysis/https-aulianza-com/9626wfhfal?form_factor=mobile

![image](https://github.com/aulianza/aulianza.com/assets/15605885/aebcd382-82c2-402e-a681-80358ef6fb6f)
<br /><br />

## Getting Started

If you are interested in running this project on your local machine, you can do so in just 3 easy steps below. Additionally, remember to update the ".env.example" file to ".env" and replace the variables with your own in the ".env" file.

### 1. Clone this template using one of the three ways:

1. Clone using git

   ```bash
   git clone https://github.com/aulianza/aulianza.com
   ```

2. Using `create-next-app`

   ```bash
   npx create-next-app -e https://github.com/aulianza/aulianza.com project-name
   ```

3. Using `degit`

   ```bash
   npx degit aulianza/aulianza.com YOUR_APP_NAME
   ```

4. Deploy to Vercel or Netlify, etc

   [![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https://github.com/aulianza/aulianza.com)
   [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/aulianza/aulianza.com)

### 2. Install dependencies

It is encouraged to use **pnpm** so the husky hooks can work properly.

```bash
pnpm install
```

### 3. Run the development server

You can start the server using this command:

```bash
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result. You can start editing the page by modifying `src/pages/index.tsx`.
<br /><br />

## License

Licensed under the [GPL-3.0 license](https://github.com/aulianza/aulianza.com/blob/master/LICENSE).
